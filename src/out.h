/**
***********************************************
***        Loco363 - LocoViewer Data        ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * LoViData
 * Output data
 */
#ifndef LOVIDATA_DATA_H_
#define LOVIDATA_DATA_H_


#include "lovi-data/data/head.h"
#include "lovi-data/data/light.h"


namespace LoViData {

struct Out {
	/** Train speed in km/h (positive when moving with cabin 1 forwards) */
	float speed = 0;
	
	/** Height of pantographs in mm (0 = full down) */
	unsigned int pantograph_1 = 0;
	unsigned int pantograph_2 = 0;
	
	/** States of cabin lights */
	Data::Light cabin_1 = Data::Light::Off;
	Data::Light cabin_2 = Data::Light::Off;
	
	/** State of all lights on heads */
	Data::Head head_1;
	Data::Head head_2;
};

} // namespace LoViData


#endif
