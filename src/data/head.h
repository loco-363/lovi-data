/**
***********************************************
***        Loco363 - LocoViewer Data        ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * LoViData::Data
 * State of all lights on single locomotive head
 */
#ifndef LOVIDATA_DATA_HEAD_H_
#define LOVIDATA_DATA_HEAD_H_


#include "lovi-data/data/pos_light.h"
#include "lovi-data/data/reflector.h"


namespace LoViData { namespace Data {

struct Head {
	/** Positional lights */
	struct {
		PosLight left;
		PosLight right;
		bool top = false;
	} poslights;
	
	/** Reflector with 2 tubes */
	Reflector reflector;
};

}} // namespace LoViData::Data


#endif
