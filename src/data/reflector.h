/**
***********************************************
***        Loco363 - LocoViewer Data        ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * LoViData::Data
 * Reflector tubes states
 */
#ifndef LOVIDATA_DATA_REFLECTOR_H_
#define LOVIDATA_DATA_REFLECTOR_H_


#include "lovi-data/data/light.h"


namespace LoViData { namespace Data {

struct Reflector {
	Light left = Light::Off;
	Light right = Light::Off;
};

}} // namespace LoViData::Data


#endif
