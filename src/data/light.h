/**
***********************************************
***        Loco363 - LocoViewer Data        ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * LoViData::Data
 * States of cabin light & reflector lamps
 */
#ifndef LOVIDATA_DATA_LIGHT_H_
#define LOVIDATA_DATA_LIGHT_H_


namespace LoViData { namespace Data {

#define EXT_ENUM_NAME Light
#define EXT_ENUM_TYPE unsigned char
#define EXT_ENUM_SCOPED
#define EXT_ENUM_DATA \
	/**
	 * Light off
	 */\
	EXT_ENUM_DEF(Off, = 0) \
	\
	/**
	 * Light on but dimmed (partial power)
	 */\
	EXT_ENUM_DEF(Dimmed, = 1) \
	\
	/**
	 * Light on (full power)
	 */\
	EXT_ENUM_DEF(Full, = 2)

#define EXT_ENUM_GENALL
#define EXT_ENUM_PRAGMAS
#include "lovi-data/lib/ext_enum/src/ext_enum.h"

}} // namespace LoViData::Data


#endif
