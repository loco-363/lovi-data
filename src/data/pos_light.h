/**
***********************************************
***        Loco363 - LocoViewer Data        ***
***-----------------------------------------***
*** Elektro-potkan  <git@elektro-potkan.cz> ***
***-----------------------------------------***
***                GNU GPLv3+               ***
***********************************************
*/


/**
 * LoViData::Data
 * Positional light lamps state
 */
#ifndef LOVIDATA_DATA_POSLIGHT_H_
#define LOVIDATA_DATA_POSLIGHT_H_


namespace LoViData { namespace Data {

struct PosLight {
	bool red = false;
	bool white = false;
};

}} // namespace LoViData::Data


#endif
